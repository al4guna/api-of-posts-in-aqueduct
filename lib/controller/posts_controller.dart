import 'package:posts/model/posts.dart';
import 'package:posts/posts.dart';


class PostsController extends ResourceController {
  PostsController(this.context);
  ManagedContext context;

  @Operation.get()
  Future<Response> getPosts() async {
    final queryPosts = Query<Post>(context);
    final posts = await queryPosts.fetch();
    return Response.ok(posts);
  }

  @Operation.get('id')
  Future<Response> getPost(@Bind.path('id') int id) async {
    final queryPost = Query<Post>(context)
      ..where((p) => p.idPost).equalTo(id);
    final post = await queryPost.fetchOne();

    return Response.ok(post);
  }

  @Operation.post()
  Future<Response> createPost(@Bind.body() Post body) async {
    final queryPost = Query<Post>(context)..values = body;
    final post = await queryPost.insert();
    return Response.ok(post);
  }

  @Operation.put('id')
  Future<Response> updatePost(@Bind.path('id') int id, @Bind.body() Post body) async {
    final queryPost = Query<Post>(context)
      ..values = body
      ..where((p) => p.idPost).equalTo(id);

    final post = await queryPost.updateOne();
    return Response.ok(post);
  }

  @Operation.delete('id')
  Future<Response> deletePost(@Bind.path('id') int id) async {
    final queryPost = Query<Post>(context)
      ..where((p) => p.idPost).equalTo(id);
    final int deleteCount = await queryPost.delete();

    if (deleteCount == 0) {
      return Response.notFound(body: 'Item not Found');
    }

    return Response.ok('Delete Item');
  }


}