import 'package:posts/posts.dart';

class Post  extends ManagedObject<_Post> implements _Post {}

class _Post {
  @primaryKey
  int idPost;

  String title;

  String description;
}