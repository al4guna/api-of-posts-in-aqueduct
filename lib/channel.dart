import 'package:posts/controller/posts_controller.dart';
import 'posts.dart';


class PostsChannel extends ApplicationChannel {
  ManagedContext context;

  @override
  Future prepare() async {
    logger.onRecord.listen((rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));

    final config = AppDB(options.configurationFilePath);
    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final persistentStore = PostgreSQLPersistentStore.fromConnectionInfo(
      config.database.username, 
      config.database.password,
      config.database.host, 
      config.database.port,
      config.database.databaseName
    );
    context = ManagedContext(dataModel, persistentStore);

  }

  @override
  Controller get entryPoint => Router()
    ..route("/posts/[:id]")
    .link(() => PostsController(context));

}

class AppDB extends Configuration {
  AppDB(String path): super.fromFile(File(path));
  DatabaseConfiguration database;
}